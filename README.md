Install Instructions:
----------------------
Unity -> Window -> Package Manager -> "+" -> Add package from git URL ... -> https://gitlab.com/toomasio/vscontrollers.git

Update instructions:
---------------------
Delete the 'packages-lock.json' file in your YourUnityProject\Packages
